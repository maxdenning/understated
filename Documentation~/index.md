---
title: Understated Documentation
---

# **Understated**
Understated is a real-time finite state machine library made for Unity. Designed to be minimal and scalable with an easy to use API which promotes clear, self-documenting code. Suitable for managing anywhere from one to many thousands of GameObjects while maintaining high performance.


## Dependencies <a name="dependencies"></a>
Understated only depends on the Unity Engine, supporting v2020.3 LTS and later.


## Installation <a name="installation"></a>
### Unity Asset Store
This package can be found on the [Unity Asset Store](https://u3d.as/2GYQ) and added to your project like any other package.

### Testing
Tests can be accessed by adding `"net.maxdenning.understated"` to the `"testables"` section of your project's package manifest (`Packages/manifest.json`), like so:

```json
"testables": [
  "net.maxdenning.understated"
]
```

You can run them by opening the Test Runner window in the Unity Editor, found under `Window -> General -> Test Runner`. 


## Features <a name="features"></a>
Understated is split into three core pieces: schemas, controllers, and clients. Together these form a state machine.
* **Schemas** describe the actions and transitions associated with states.
* **Clients** are data with an associated state which are transformed by a **Schema**.
* **Controllers** transform **Clients** using a **Schema**.


### Schemas
`Understated.Schema<TStateKey, TEventKey, TData>`

Schemas describe the actions and transitions of a finite state machine using various types of listeners and callbacks. Schemas are themselves stateless and are stored as a ScriptableObject asset, meaning one instance can be shared amongst many controllers and clients.

#### Actions
Actions are methods which transform the data of clients, and can be linked to the following events:
* Enter: called once when a client enters the associated state
* Exit: called once when a client exits the associated state
* Transition: called once when a client transitions from one state to another
* User Events: called once when a user event of the associated type is received by the controller
* Update: called repeatedly, in sync with the Unity MonoBehaviour Update method
* Fixed Update: called repeatedly, in sync with Unity MonoBehaviour FixedUpdate method
* Late Update: called repeatedly, in sync with the Unity MonoBehaviour LateUpdate method


Actions can be optionally associated with specific states.
```csharp
// this listener is invoked when any client enters any state
actions.enter.AddListener((MyData data, MyStates enter) => {});

// this listener is invoked when any client transitions between any two states
actions.transition.AddListener((MyData data, MyStates from, MyStates to) => {});
```
```csharp
// this listener is invoked when any client enters the MyStates.Idle state
actions.Enter(MyStates.Idle).AddListener((MyData data, MyStates enter) => {});

// this listener is invoked when any client transitions from MyStates.Idle to MyStates.Walking
actions.From(MyStates.Idle).To(MyStates.Walking).AddListener((MyData data, MyStates from, MyStates to) => {});
```

Additionally, actions can be associated with user events and a context type.  
```csharp
// when the ChangeColour event is received, all clients set their colour to the given value
actions.On<Color>(MyEvents.ChangeColour)
       .AddListener((MyData data, Color context) => data.colour = context);

// when the ChangeColour event is received, all idle clients set their colour to the given value
actions.Event(MyStates.Idle)
       .On<Color>(MyEvents.ChangeColour)
       .AddListener((MyData data, Color context) => data.colour = context);
```

Both schemas and controllers keep sets of actions, allowing more granularity in behaviour.
```csharp
// this listener is invoked when any client using this schema enters any state
schema.actions.enter.AddListener((MyData data, MyStates enter) => {});

// this listener is invoked when any client using this controller enters any state
controller.actions.enter.AddListener((MyData data, MyStates enter) => {});
```

When associated with a schema or a multi-client controller, actions can operate over a collection of client data or on a single instance at a time. However, when associated with a single-client controller, they receive only that client's data. 
```csharp
// for a schema
schema.enter.AddListener((ClientCollection<MyData> collection, MyStates enter) => {
    foreach (MyData data in collection)
    {
    }
});
// or
schema.enter.AddListener((MyData data, MyStates enter) => {});

// for a multi-client controller
mulitController.enter.AddListener((ClientCollection<MyData> collection, MyStates enter) => {
    foreach (MyData data in collection)
    {
    }
});
// or
mulitController.enter.AddListener((MyData data, MyStates enter) => {});

// for a single-client controller
singleController.enter.AddListener((MyData data, MyStates enter) => {});
```

#### Transitions
Transition triggers and selectors indicate when a client should change state based on its data or on external events.

**Triggers** connect two states and indicate a change is necessary when their condition is met.

* **Polled Triggers**: the condition is checked once each frame.
    ```csharp
    transitions.From(WaterStates.Liquid)
               .To(WaterStates.Gas)
               .When((data) => data.temperature >= 100f);
    ```

* **Event Triggers**: an optional condition is checked when an event is received. Event context can optionally be received by the trigger.
    ```csharp
    transitions.From(PlayerStates.Standing)
               .To(PlayerStates.Jumping)
               .On(PlayerEvents.PressJump)
               .Always<InputAction.CallbackContext>();
    ```
    ```csharp
    transitions.From(PlayerStates.Standing)
               .To(PlayerStates.Jumping)
               .On(PlayerEvents.PressJump)
               .When((data) => data.isOnTheGround);
    ```
    ```csharp
    transitions.From(PlayerStates.Standing)
               .To(PlayerStates.Jumping)
               .On(PlayerEvents.PressJump)
               .When<InputAction.CallbackContext>(
                   (PlayerData data, in InputAction.CallbackContext context) =>
                       context.action.phase == InputActionPhase.Performed;
                );
    ```

* **Timeout Triggers**: the state is changed after a number of seconds, either fixed or variable.
    ```csharp
    transitions.From(AlarmStates.Silent)
               .To(AlarmStates.Ringing)
               .After(60f);
    ```
    ```csharp
    transitions.From(AlarmStates.Silent)
               .To(AlarmStates.Ringing)
               .After((data) => data.duration);
    ```

**Selectors** connect one state to many states and indicates a change is necessary when some condition is met and explicitly selects what the next state should be.

* **Polled Selectors**: the selector is checked once each frame.
    ```csharp
    transitions.From(WaterStates.Liquid)
               .To((WaterData data, ref WaterStates nextState) => {
                   if (data.temperature >= 100f)
                   {
                       nextState = WaterStates.Gas;
                       return true;
                   }
                   else if (data.temperature <= 0f)
                   {
                       nextState = WaterStates.Solid;
                       return true;
                   }
                   return false;
                })
               .Always();
    ```

* **Event Selectors**: the selector is checked when an event is received.
    ```csharp
    transitions.From(BreadStates.Cooking)
               .To<ToasterContext>(
                   (BreadData data, in ToasterContext context, ref BreadStates nextState) => {
                       if (data.temperature > 100f)
                       {
                           nextState = BreadStates.Burnt;
                       }
                       else
                       {
                           nextState = BreadStates.Toasted;
                       }
                       return true;
                   })
               .On(BreadEvents.ToasterPopped);
    ```

* **Timeout Selectors**: the selector is checked after some amount of time, either fixed or variable.
    ```csharp
    transitions.From(AlarmStates.Silent)
               .To((AlarmData data, ref AlarmStates nextState) => {
                   switch(data.mode)
                   {
                       case AlarmModes.SoundOnly:
                           nextState = AlarmStates.Ringing;
                           break;
                       case AlarmModes.LightsOnly:
                           nextState = AlarmStates.Flashing;
                           break;
                       default:
                           nextState = AlarmStates.Both;
                           break;
                   }
                   return true;
                })
               .After(60f);  // or .After((data) => data.duration);
    ```


### Controllers & Clients
Controllers transform clients using schemas. Since schemas are a shared asset, multiple controllers can use the same schema instance. Each controller defines its own client type optimised for its use case. A client must only be registered with one controller.

Some controller and client types are MonoBehaviours that can be attached to a GameObject. These types can be found in the `Understated.Components` namespace.
These controllers are tied to the Unity update loop and synchronise client state changes in one of Update, FixedUpdate, or LateUpdate depending on the user's choice.

* **Single Client Controller** manages one client.
    * Controller:
        * `Understated.SingleClientController<TStateKey, TEventKey, TData>`
        * `Understated.Components.SingleClientController<TStateKey, TEventKey, TData>`
    * Client:
        * `Understated.SingleClientController<TStateKey, TEventKey, TData>.Client`

* **Multi-Client Controller** manages any number of clients.
    * Controller:
        * `Understated.MultiClientController<TStateKey, TEventKey, TData>`
        * `Understated.Components.MultiClientController<TStateKey, TEventKey, TData>`
    * Client:
        * `Understated.MultiClientController<TStateKey, TEventKey, TData>.Client`
        * `Understated.Components.MultiClientController<TStateKey, TEventKey, TData>.Client`

Both `SingleClientController` and `MultiClientController` are single threaded, so they can be deployed to WebGL targets.


### Not Supported
* Composite states, substates, and nested state machines


## Quick Start Templates <a name="quick_start"></a>
To add a finite state machine to your project you will need to create a schema ScriptableObject asset and a controller MonoBehaviour.

Create a schema class using the template below:
```csharp
// MySchema.cs
using Understated;

public enum MyStates
{
}

public enum MyEvents
{
}

[System.Serializable]
public class MyData
{
}

[CreateAssetMenu(fileName = "MySchema", menuName = "Understated/MySchema")]
public class MySchema : Understated.Schema<MyStates, MyEvents, MyData>
{
    public MySchema()
    {
        // define your state schema's actions and transitions
    }
}
```

Depending on your use-case create a single-client controller or a multi-client controller.

Single-client controller MonoBehaviours come with a client, so you only need to subclass the controller type. Whereas multi-client controller MonoBehvaiours require you to subclass both the controller and client types.

Single-client controller template:
```csharp
// MySingleController.cs
public class MySingleController : Understated.Components.SingleClientController<MyStates, MyEvents, MyData>
{
}
```

Mutli-client controller template:
```csharp
// MyMultiController.cs
public class MyMultiController : Understated.Components.MultiClientController<MyStates, MyEvents, MyData>
{
}
```
```csharp
// MyMultiClient.cs
public class MyMultiClient : MyMultiController.Client
{
}
```

In the Unity Editor create an instance of your schema asset using the Project window's Create menu and add your controller (and client, if using multi-client) to a GameObject. Connect them by dragging and dropping references in the inspector.


## Tutorial <a name="tutorial"></a>
The complete code for this tutorial can be found under `Samples/GettingStarted`.

The goal of this guide is to set up the state machine pictured below. It will control an agent (GameObject) which moves around randomly and changes colour when you press a button. After that you will create two scenes, one containing a single agent and another containing many agents all using the same schema.

![tutorial state diagram](images/getting-started-diagram.png)

First you will need a Unity 2D project with the Understated package installed.


### Setting up a Schema
Create a script named `MyMachineSchema.cs` to contain our schema implementation.

All schemas require an enum type for state labels, an enum type for event labels, and a class type for client data.

```csharp
// MyMachineSchema.cs
using UnityEngine;
using Understated;

// These are all the possible states that a client of the state machine can be in.
public enum MyStates
{
    Idle = 0,  // defines the default state (i.e. default(MyStates) == MyStates.Idle)
    Moving
}

// These are the names of user-generated events which we will emit elsewhere in the code.
public enum MyEvents
{
    ChangeColour
}

[System.Serializable]
public class MyData
{
    public Transform transform;  // used to move the agent around
    public SpriteRenderer sprite;  // used to change the colour of the agent
    public Vector3 moveSpeed = new Vector3(4f, 4f, 0f);  // the speed of the agent
    public Vector3 moveDirection = Vector3.up;  // the direction of the agent
}
```

With these in place we can create our schema ScriptableObject type.

```csharp
// MyMachineSchema.cs

// use CreateAssetMenu to add an option to the create menu in the Unity Editor project window
[CreateAssetMenu(fileName = "MyMachineSchema", menuName = "Understated/GettingStarted/MyMachineSchema")]
public class MyMachineSchema : Understated.Schema<MyStates, MyEvents, MyData>
{
}
```

Now we can start to create our actions and transition triggers. To get our agent to move around in different directions when it is `Moving` we can define the following methods:

```csharp
// MyMachineSchema.cs

public static void TurnAction(MyData data, MyStates state)
{
    // randomly pick a new direction
    data.moveDirection = Vector3.Normalize(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f));
}

public static void MoveAction(MyData data, MyStates state)
{
    // move in the direction the agent has chosen
    data.transform.position += Vector3.Scale(data.moveSpeed, data.moveDirection) * Time.deltaTime;
}
```

These state actions can then be added to the schema along with some transition triggers to change between `Idle` to `Moving` every few seconds.

```csharp
// MyMachineSchema.cs

public class MyMachineSchema : Understated.Schema<MyStates, MyEvents, MyData>
{
    public MyMachineSchema()
    {
        actions.Enter(MyStates.Moving).AddListener(TurnAction);
        actions.Update(MyStates.Moving).AddListener(MoveAction);

        // log whenever an agent enters a new state
        actions.enter.AddListener((MyData data, MyStates state) => Debug.Log($"{data.transform.name} enter {state}"));

        transitions.From(MyStates.Idle).To(MyStates.Moving).After(1f);  // 1 second
        transitions.From(MyStates.Moving).To(MyStates.Idle).After(3f);  // 3 seconds
    }
}
```

To change colour whenever we receive a `ChangeColour` event we can create the following method:

```csharp
// MyMachineSchema.cs

public static void ChangeColourAction(MyData data, MyStates state, in Color eventContext)
{
    // set the agent colour to the given value
    data.sprite.color = eventContext;
}
```

Which can be added to the schema like so:

```csharp
// MyMachineSchema.cs

public class MyMachineSchema : Understated.Schema<MyStates, MyEvents, MyData>
{
    public MyMachineSchema()
    {
        // ...

        // change colour only when Idle and a ChangeColor event is received
        actions.Event(MyStates.Idle).On<Color>(MyEvents.ChangeColour).AddListener(ChangeColourAction);

        // ...
    }
    // ...
}
```

With our schema set up, we need to create an instance of it in our project. In the Unity editor go to the project window, right-click and navigate to `Create -> Understated -> Getting Started -> MyMachineSchema`. This will create a ScriptableObject asset we can connect to our controllers later.


### Single Client Controller
If we only want one agent connected to this schema we can use a `SingleClientController`. Create a new file named `MySingleController.cs` for our controller MonoBehaviour. In this file create a subclass of `SingleClientController` using our state, event, and data types.

```csharp
// MySingleController.cs

public class MySingleController : Understated.Components.SingleClientController<MyStates, MyEvents, MyData>
{
}
```

In the editor create a new Scene and add a 2D circle sprite GameObject named "Agent". Select this GameObject and add the `MySingleController` component. Drag the schema asset we created earlier to the slot in `MySingleController` and drag references to the Agent's `Transform` and `SpriteRenderer` to the slots under `Client -> Data`.

Finally we need some code to emit the `ChangeColour` event when a key is pressed. Create a new script named `InputHandler.cs` and add the following:

```csharp
// InputHandler.cs
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    // use the generic type for all controller MonoBehaviours
    [SerializeField]
    private Understated.ControllerComponent<MyStates, MyEvents, MyData> controller; 

    void Update()
    {
        // emit the event whenever space is pressed
        if (Input.GetKeyDown("space"))
        {
            Color randomColour = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
            controller.SendEvent(MyEvents.ChangeColour, randomColour);
        }
    }
}
```

Add an `InputHandler` component to the "Agent" GameObject and drag and drop a reference to the `MySingleController` component into the `controller` slot.

![Unity editor example](images/getting-started-single-agent.png)

Set the scene's "Main Camera" size to 25.

Click play on the scene and watch the Agent randomly move around the play area. Press space and see that the agent changes colour, but only when it is not moving.

### Multi-Client Controller
To efficiently connect multiple agents to this schema we need a `MultiClientController`.
Create scripts named `MyMultiController.cs` and `MyMultiClient.cs` for our controller and client MonoBehaviours.

```csharp
// MyMultiController.cs

public class MyMultiController : Understated.Components.MultiClientController<MyStates, MyEvents, MyData>
{
}
```
```csharp
// MyMultiClient.cs

public class MyMultiClient : MyMultiController.Client
{
}
```

Create a new scene and add an empty GameObject named "AgentManager". Add the `MyMultiController` to "AgentManager" and drag a reference to the schema into the slot on the controller.

Create a circle sprite GameObject named "Agent" and add the `MyMultiClient` component to it. Drag `Transform` and `SpriteRenderer` references into the slots on the client.

![Unity editor example](images/getting-started-multi-agent.png)

Duplicate "Agent" a few times and then select "AgentManager" and add each of the agents to the "Serialised Clients" list.

To have the agents change colour when you press space we can re-use the `InputHandler` component we made earlier. Add the `InputHandler` component to "AgentManager" and drag a reference to the `MyMultiController` component into the `controller` slot.

![Unity editor example](images/getting-started-multi-agent-manager.png)

Set the scene's "Main Camera" size to 25.

Click play on the scene and watch the agents randomly walk around the play area. Press space and see that all the agents change to the same colour but only when they are not moving.


## Examples <a name="examples"></a>
See the packaged samples for more detail.

#### Getting Started
Complete code for the tutorial section of the documentation.

#### Platformer 2D
An example character controller for a single player 2D platformer using the new input system.
