using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Understated;

namespace Platformer2D
{
    [Serializable]
    public class MovementData
    {
        [Serializable]
        public class JumpCharacteristics
        {
            public float speed;
            public float maxHoldDuration;  // seconds
            public AnimationCurve velocityCurve;
        }

        [SerializeField]
        public PlayerMovement movement;

        [SerializeField]
        public PlayerWallSlideTriggers wallSlide;

        [HideInInspector]
        public Vector2 moveDirection = Vector2.zero;

        [SerializeField]
        public float walkSpeed = 10;

        [SerializeField]
        public float fallSpeed = 5;

        [SerializeField]
        public List<JumpCharacteristics> jumps;

        [HideInInspector]
        public int jumpsPerformed = 0;

        [HideInInspector]
        public bool isHoldingJump = false;
    }

    [CreateAssetMenu(fileName = "PlayerMovementSchema", menuName = "Understated/Platformer2D/PlayerMovementSchema")]
    public class PlayerMovementSchema : Understated.Schema<PlayerMovementSchema.States, PlayerMovementSchema.Events, MovementData>
    {
        public enum States
        {
            Idle = 0, Fall, Walk, Jump, WallSlide, Attack,
        }

        public enum Events
        {
            JumpInputBegin, JumpInputEnd, AttackInput,
        }

        public PlayerMovementSchema()
        {
            //
            actions.Enter(States.Idle).AddListener(ResetJumps);
            actions.Update(States.Idle).AddListener(StopMoving);

            actions.Update(States.Fall).AddListener(MoveInAir);

            actions.Update(States.Walk).AddListener(MoveOnFloor);

            actions.AddListeners(States.Jump, new JumpStateActions());

            actions.Enter(States.WallSlide).AddListener(ResetJumps);
            actions.Update(States.WallSlide).AddListener(SlowFall);

            //
            actions.transition.AddListener((MovementData data, States from, States to) => Debug.Log($"{from} -> {to}"));

            //
            transitions.From(States.Idle).To(States.Fall).When(IsFalling);
            transitions.From(States.Idle).To(States.Walk).When((data) => data.movement.isGrounded && data.moveDirection.x != 0);
            transitions.From(States.Idle).To(States.Jump).On(Events.JumpInputBegin).When<InputAction.CallbackContext>(IsJumpAvailable);
            transitions.From(States.Idle).To(States.Attack).On(Events.AttackInput).Always<InputAction.CallbackContext>();

            transitions.From(States.Fall).To(States.Idle).When((data) => data.movement.isGrounded);
            transitions.From(States.Fall).To(States.Jump).On(Events.JumpInputBegin).When<InputAction.CallbackContext>(IsJumpAvailable);
            transitions.From(States.Fall).To(States.WallSlide).When((data) => data.wallSlide.isWallLeft || data.wallSlide.isWallRight);

            transitions.From(States.Walk).To(States.Idle).When((data) => data.movement.isGrounded && data.moveDirection.x == 0);
            transitions.From(States.Walk).To(States.Fall).When(IsFalling);
            transitions.From(States.Walk).To(States.Jump).On(Events.JumpInputBegin).When<InputAction.CallbackContext>(IsJumpAvailable);
            transitions.From(States.Walk).To(States.Attack).On(Events.AttackInput).Always<InputAction.CallbackContext>();

            transitions.From(States.Jump).To(States.Fall).When(IsFalling);
            transitions.From(States.Jump).To(States.Jump).On(Events.JumpInputBegin).When<InputAction.CallbackContext>(IsJumpAvailable);

            transitions.From(States.WallSlide).To(States.Idle).When((data) => data.movement.isGrounded);
            transitions.From(States.WallSlide).To(States.Fall).When((data) => !data.wallSlide.isWallLeft && !data.wallSlide.isWallRight);
            transitions.From(States.WallSlide).To(States.Jump).On(Events.JumpInputBegin).When<InputAction.CallbackContext>(WallSlideToJump);

            transitions.From(States.Attack).To(States.Idle).After(0.5f);
        }

        private static bool IsFalling(MovementData data)
        {
            return !data.movement.isGrounded && data.movement.linearVelocity.y < 0 && !data.isHoldingJump;
        }

        private static bool IsJumpAvailable(MovementData data, in InputAction.CallbackContext context)
        {
            return data.jumpsPerformed < data.jumps.Count;
        }

        private static bool WallSlideToJump(MovementData data, in InputAction.CallbackContext context)
        {
            // if jump is available and movement direction opposes direction of wall
            return IsJumpAvailable(data, context) && ((data.moveDirection.x >= 0 && data.wallSlide.isWallLeft) || (data.moveDirection.x <= 0 && data.wallSlide.isWallRight));
        }

        private static void ResetJumps(MovementData data, States state)
        {
            data.jumpsPerformed = 0;
        }

        private static void StopMoving(MovementData data, States state)
        {
            data.movement.AccelerateTowards(Vector2.zero, Time.deltaTime);
        }

        private static void MoveInAir(MovementData data, States state)
        {
            // if smaller magnitude or in different direction then accelerate towards
            float move = data.moveDirection.x * data.fallSpeed;
            if (Mathf.Abs(data.movement.linearVelocity.x) < data.fallSpeed || (data.movement.linearVelocity.x * move) < 0)
            {
                data.movement.AccelerateXTowards(move, Time.deltaTime);
            }
        }

        private static void MoveOnFloor(MovementData data, States state)
        {
            data.movement.AccelerateXTowards(data.moveDirection.x * data.walkSpeed, Time.deltaTime);
        }

        private static void SlowFall(MovementData data, States state)
        {
            data.movement.linearVelocity.y = 0;
        }

        private class JumpStateActions : PlayerMovementSchema.IActionAdder
        {
            private float timer = 0;
            private MovementData.JumpCharacteristics jump = null;

            public void AddListeners(ActionDispatcher<States, Events, ClientCollection<MovementData>> actions, States state)
            {
                actions.Enter(state).AddListener(Enter);
                actions.Update(state).AddListener(Jump);
                actions.Update(state).AddListener(MoveInAir);
                actions.Event(state).On<InputAction.CallbackContext>(Events.JumpInputEnd).AddListener(HoldComplete);
                actions.Exit(state).AddListener(HoldComplete);
            }

            private void Enter(MovementData data, States state)
            {
                timer = 0;
                jump = data.jumps[data.jumpsPerformed];
                data.jumpsPerformed++;
                data.isHoldingJump = true;
            }

            private void Jump(MovementData data, States state)
            {
                if (data.isHoldingJump)
                {
                    data.movement.linearVelocity.y = jump.velocityCurve.Evaluate(timer) * jump.speed;

                    if (timer >= jump.maxHoldDuration)
                    {
                        HoldComplete(data, state);
                    }
                    timer += Time.deltaTime;
                }
            }

            private static void HoldComplete(MovementData data, States state)
            {
                data.isHoldingJump = false;
            }
        }
    }
}
