using System.Collections.Generic;
using UnityEngine;

namespace Platformer2D
{
    public class PlayerMovement : MonoBehaviour
    {
        private const float minMoveDistance = 0.01f;
        private const float castPaddingRadius = 0.01f;

        [SerializeField]
        public Vector2 maxSpeed = new Vector2(20, 20);

        [SerializeField]
        public Vector2 acceleration = Vector2.one;

        [SerializeField]
        public float gravityScale = 1;

        [HideInInspector]
        public bool isGrounded { get; private set; } = false;

        [HideInInspector]
        public Vector2 linearVelocity = Vector2.zero;

        [SerializeField]
        private Vector2 minGroundNormal = new Vector2(-1.0f, 0.7f);

        private Vector2 groundNormal = Vector2.up;

        [SerializeField]
        private Rigidbody2D playerRigidBody;

        private ContactFilter2D filter;

        private List<RaycastHit2D> collisions = new List<RaycastHit2D>(0);


        public void AccelerateTowards(Vector2 value, float timeStep)
        {
            AccelerateXTowards(value.x, timeStep);
            AccelerateYTowards(value.y, timeStep);
        }

        public void AccelerateXTowards(float value, float timeStep)
        {
            linearVelocity.x = Mathf.MoveTowards(linearVelocity.x, value, acceleration.x * timeStep);
        }

        public void AccelerateYTowards(float value, float timeStep)
        {
            linearVelocity.y = Mathf.MoveTowards(linearVelocity.y, value, acceleration.y * timeStep);
        }

        private void Start()
        {
            filter.useTriggers = false;
            filter.useLayerMask = true;
            filter.layerMask = ~(1 << gameObject.layer);
        }

        private void FixedUpdate()
        {
            // gravity
            linearVelocity += gravityScale * Physics2D.gravity * Time.fixedDeltaTime;

            // drag
            linearVelocity.x = Mathf.Lerp(linearVelocity.x, 0, (isGrounded ? 1.25f : 0.25f) * Time.fixedDeltaTime);
            linearVelocity.y = Mathf.Lerp(linearVelocity.y, 0, 0.25f * Time.fixedDeltaTime);

            // limit top speed
            linearVelocity.x = Mathf.Clamp(linearVelocity.x, -maxSpeed.x, maxSpeed.x);
            linearVelocity.y = Mathf.Clamp(linearVelocity.y, -maxSpeed.y, maxSpeed.y);

            // move
            if (linearVelocity.magnitude > minMoveDistance)
            {
                Move(Time.fixedDeltaTime);
            }
        }

        // move rigid body by velocity * timeStep
        private void Move(float timeStep)
        {
            Vector2 move = linearVelocity * timeStep;
            bool grounded = false;
            groundNormal = Vector2.up;

            // move vertically
            Vector2 moveVert = TestMoveCollisions(playerRigidBody, filter, castPaddingRadius, collisions, Vector2.up * move.y, ref linearVelocity, minGroundNormal, ref groundNormal, ref grounded);
            playerRigidBody.position += moveVert;
            collisions.Clear();

            // move along ground plane
            Vector2 groundPlane = new Vector2(groundNormal.y, -groundNormal.x);
            Vector2 moveHorz = TestMoveCollisions(playerRigidBody, filter, castPaddingRadius, collisions, groundPlane * move.x, ref linearVelocity, minGroundNormal, ref groundNormal, ref grounded);
            playerRigidBody.position += moveHorz;
            collisions.Clear();

            isGrounded = grounded;

            //DEBUG
            Debug.DrawRay(playerRigidBody.position, groundNormal * 2, Color.red);
            Debug.DrawRay(playerRigidBody.position, (moveHorz + moveVert).normalized * 2, Color.green);
        }

        // tests the given move and returns adjusted move step to avoid collisions
        private static Vector2 TestMoveCollisions(
            Rigidbody2D body,
            ContactFilter2D filter,
            float castPaddingRadius,
            List<RaycastHit2D> collisions,
            Vector2 move,
            ref Vector2 velocity,
            Vector2 minGroundNormal,
            ref Vector2 groundNormal,
            ref bool isGrounded)
        {
            Vector2 direction = move.normalized;
            float distance = move.magnitude;

            int start = collisions.Count;
            int count = body.Cast(direction, filter, collisions, distance + castPaddingRadius);
            for (int i = start; i < start + count; i++)
            {
                RaycastHit2D hit = collisions[i];

                if (hit.normal.x >= minGroundNormal.x && hit.normal.y >= minGroundNormal.y)
                {
                    isGrounded = true;
                    groundNormal = hit.normal;
                }

                float projection = Vector2.Dot(velocity, hit.normal);
                if (projection < 0) 
                {
                    velocity -= projection * hit.normal;
                }

                distance = Mathf.Min(distance, hit.distance - castPaddingRadius);

                //DEBUG
                Debug.DrawRay(hit.point, hit.normal, Color.blue);
            }

            return direction * distance;
        }
    }
}
