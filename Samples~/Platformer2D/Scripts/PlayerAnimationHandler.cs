using UnityEngine;
using States = Platformer2D.PlayerMovementSchema.States;

namespace Platformer2D
{
    public class PlayerAnimationHandler : MonoBehaviour
    {
        [SerializeField]
        private Animator animator;

        [SerializeField]
        private PlayerMovement movement;

        [SerializeField]
        private PlayerWallSlideTriggers wallSlide;

        [SerializeField]
        private PlayerMovementController controller;

        [SerializeField]
        private Transform flipYTransform;

        [SerializeField]
        private ParticleSystem bottomDust;

        [SerializeField]
        private ParticleSystem backDust;

        private void Start()
        {
            controller.actions.Enter(States.WallSlide).AddListener(FlipAgainstWall);
            controller.actions.transition.AddListener(PlayAnimation);
        }

        private void Update()
        {
            Vector3 scale = flipYTransform.localScale;
            if (controller.client.state != States.WallSlide && ((movement.linearVelocity.x < 0 && scale.x > 0) || (movement.linearVelocity.x > 0 && scale.x < 0)))
            {
                flipYTransform.localScale = scale * new Vector2(-1, 1);
                
                if (movement.isGrounded)
                {
                    bottomDust.Play();
                }
            }
        }

        private void FlipAgainstWall(MovementData data, States enter)
        {
            Vector3 scale = flipYTransform.localScale;
            if ((wallSlide.isWallRight && scale.x > 0) || (wallSlide.isWallLeft && scale.x < 0))
            {
                flipYTransform.localScale = scale * new Vector2(-1, 1);
            }
        }

        private void PlayAnimation(MovementData data, States from, States to)
        {
            if (from == States.WallSlide)
            {
                backDust.Stop();
            }

            switch (to)
            {
                case States.Idle:
                    if (from == States.Fall)
                    {
                        animator.Play("Landing");
                        bottomDust.Play();
                    }
                    else
                    {
                        animator.Play("Idle");
                    }
                    break;

                case States.Fall:
                    animator.Play("Fall");
                    break;

                case States.Walk:
                    animator.Play("Walk");
                    break;
                
                case States.Jump:
                    animator.Play("Jump");
                    if (from != States.WallSlide)
                    {
                        bottomDust.Play();
                    }
                    break;

                case States.WallSlide:
                    animator.Play("WallSlide");
                    backDust.Play();
                    break;
                
                case States.Attack:
                    animator.Play("Punch");
                    break;
            }
        }
    }
}
