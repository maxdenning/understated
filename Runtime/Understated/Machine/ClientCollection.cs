using System;
using System.Collections;
using System.Collections.Generic;

namespace Understated
{
    // wraps ArraySegment because of limited Unity C# version support
    public struct ClientCollection<T> : IReadOnlyList<T>
    {
        public struct Enumerator : IEnumerator<T>
        {
            private ClientCollection<T> collection;
            private int index;

            public Enumerator(ClientCollection<T> collection)
            {
                this.collection = collection;
                this.index = -1;
            }

            public bool MoveNext()
            {
                return ++index < collection.Count;
            }

            public void Reset()
            {
                index = -1;
            }

            void IDisposable.Dispose()
            {
            }

            public T Current => collection[index];
            object IEnumerator.Current => Current;
        }

        public int Count => collection.Count;

        public T this[int index] => collection.Array[collection.Offset + index];

        private ArraySegment<T> collection;

        internal ClientCollection(ArraySegment<T> collection)
        {
            this.collection = collection;
        }

        public static implicit operator ClientCollection<T>(T[] collection)
        {
            return new ClientCollection<T>(new ArraySegment<T>(collection, 0, collection.Length));
        }

        public static implicit operator ClientCollection<T>(ArraySegment<T> collection)
        {
            return new ClientCollection<T>(collection);
        }

        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new Enumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Enumerator(this);
        }
    }
}
