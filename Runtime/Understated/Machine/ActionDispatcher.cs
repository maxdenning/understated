using System;
using System.Collections.Generic;

namespace Understated
{
    /// <summary>
    /// Event action delegate type. Used by EventActionListener for user event actions.
    /// For example:
    ///     schema.actions.Event<int>(MyEvents.WithInt).AddListener((MyData data, MyState state, in int context) => {});
    /// </summary>
    /// <param name="arg0">First argument, usually used for client data.</param>
    /// <param name="arg1">Second argument, usually used for client state.</param>
    /// <param name="arg2">Final argument, used for event context</param>
    /// <typeparam name="TArg0">First argument type.</typeparam>
    /// <typeparam name="TArg1">Second argument type.</typeparam>
    /// <typeparam name="TEventContext">Final argument type.</typeparam>
    /// <returns>void</returns>
    public delegate void EventAction<TArg0, TArg1, TEventContext>(TArg0 arg0, TArg1 arg1, in TEventContext arg2);

    /// <summary>
    /// Helper interface to simplify adding many related state action events to an ActionDispatcher.
    /// For example, if the state actions are not static methods and are all members of the same
    /// class then implementing this interface may be easier than adding each method individually
    /// elsewhere.
    /// </summary>
    /// <typeparam name="TStateKey">State label type.</typeparam>
    /// <typeparam name="TEventKey">Event label type.</typeparam>
    /// <typeparam name="TParam">Data parameter type passed to all listeners.</typeparam>
    public interface IActionAdder<TStateKey, TEventKey, TParam>
        where TStateKey : struct
        where TEventKey : struct
    {
        // void AddListeners(StateActionDispatcher<TStateKey, TEventKey, TParam> actions, TStateKey state);
        void AddListeners(ActionDispatcher<TStateKey, TEventKey, TParam> actions, TStateKey state);
    }

    /// <summary>
    /// Helper interface to simplify removing many related state action events from an
    /// ActionDispatcher. For example, if the state actions are not static methods and are all
    /// members of the same class then implementing this interface may be easier than removing
    /// each method individually elsewhere.
    /// </summary>
    /// <typeparam name="TStateKey">State label type.</typeparam>
    /// <typeparam name="TEventKey">Event label type.</typeparam>
    /// <typeparam name="TParam">Data parameter type passed to all listeners.</typeparam>
    public interface IActionRemover<TStateKey, TEventKey, TParam>
        where TStateKey : struct
        where TEventKey : struct
    {
        // void RemoveListeners(StateActionDispatcher<TStateKey, TEventKey, TParam> actions, TStateKey state);
        void RemoveListeners(ActionDispatcher<TStateKey, TEventKey, TParam> actions, TStateKey state);
    }

    /// <summary>
    /// Wraps state actions to provide a similar interface to UnityActions and UnityEvents.
    /// Delegate type: void (TArg0 arg0, TArg1 arg1).
    /// </summary>
    /// <typeparam name="TArg0">First argument type.</typeparam>
    /// <typeparam name="TArg1">Second argument type.</typeparam>
    public class ActionListener<TArg0, TArg1> : DelegateWrapper<Action<TArg0, TArg1>>
    {
        internal void Invoke(TArg0 arg0, TArg1 arg1) => wrappedDelegate?.Invoke(arg0, arg1);
    }

    /// <summary>
    /// Wraps state actions to provide a similar interface to UnityActions and UnityEvents.
    /// Delegate type: void (TArg0 arg0, TArg1 arg1, TArg2 arg2).
    /// </summary>
    /// <typeparam name="TArg0">First argument type.</typeparam>
    /// <typeparam name="TArg1">Second argument type.</typeparam>
    /// <typeparam name="TArg2">Third argument type.</typeparam>
    public class ActionListener<TArg0, TArg1, TArg2> : DelegateWrapper<Action<TArg0, TArg1, TArg2>>
    {
        internal void Invoke(TArg0 arg0, TArg1 arg1, TArg2 arg2) => wrappedDelegate?.Invoke(arg0, arg1, arg2);
    }

    /// <summary>
    /// Wraps state event actions to provide a similar interface to UnityActions and UnityEvents.
    /// Delegate type: void (TArg0 arg0, TArg1 arg1, TEventContext arg2).
    /// </summary>
    /// <typeparam name="TArg0">First argument type.</typeparam>
    /// <typeparam name="TArg1">Second argument type.</typeparam>
    internal class EventActionListener<TArg0, TArg1> : GenericDelegateWrapper
    {
        public void Invoke<TEventContext>(TArg0 arg0, TArg1 arg1, in TEventContext context)
        {
            if (wrappedDelegate is EventAction<TArg0, TArg1, TEventContext> action)
            {
                action.Invoke(arg0, arg1, context);
            }
            else if (wrappedDelegate != null)
            {
                // enforce one TEventContext type per TEventKey
                throw new ArgumentException("TEventContext does not match the type associated with existing delegates for this event", "TEventContext");
            }
        }
    }

    /// <summary>
    /// Public API for EventActionListener used simplify generic arguments.
    /// </summary>
    /// <typeparam name="TArg0">First argument type.</typeparam>
    /// <typeparam name="TArg1">Second argument type.</typeparam>
    /// <typeparam name="TEventContext">Event context type</typeparam>
    public readonly struct EventActionListenerProxy<TArg0, TArg1, TEventContext>
    {
        private readonly EventActionListener<TArg0, TArg1> wrapper;

        internal EventActionListenerProxy(EventActionListener<TArg0, TArg1> wrapper)
        {
            this.wrapper = wrapper;
        }

        public EventAction<TArg0, TArg1, TEventContext> AddListener(EventAction<TArg0, TArg1, TEventContext> listener)
        {
            return wrapper.AddListener<EventAction<TArg0, TArg1, TEventContext>>(listener);
        }

        public EventAction<TArg0, TArg1, TEventContext> AddListener(Action<TArg0, TArg1> listener)
        {
            return wrapper.AddListener<EventAction<TArg0, TArg1, TEventContext>>((TArg0 arg0, TArg1 arg1, in TEventContext arg2) => listener.Invoke(arg0, arg1));
        }

        public void RemoveListener(EventAction<TArg0, TArg1, TEventContext> listener) => wrapper.RemoveListener<EventAction<TArg0, TArg1, TEventContext>>(listener);
        public void RemoveAllListeners() => wrapper.RemoveAllListeners();
    }

    /// <summary>
    /// Collection of events intended to transform data depending on its state.
    /// </summary>
    /// <typeparam name="TStateKey">State label type.</typeparam>
    /// <typeparam name="TEventKey">Event label type.</typeparam>
    /// <typeparam name="TParam">Data parameter type passed to all listeners.</typeparam>
    public class ActionDispatcher<TStateKey, TEventKey, TParam>
        where TStateKey : struct
        where TEventKey : struct
    {
        //TODO: could enum flags be used? for generic enter/exit etc, could a value which matches all flags (i.e. 11111111) be used?
        //TODO: keep two kinds of events? events with and without data?
        //TODO: create IActionDispatcher for this and other dispatchers? e.g. client action dispatcher?

        //
        public readonly struct EventActionAPIProxy
        {
            private readonly ActionDispatcher<TStateKey, TEventKey, TParam> dispatcher;
            private readonly TStateKey stateKey;

            internal EventActionAPIProxy(ActionDispatcher<TStateKey, TEventKey, TParam> dispatcher, TStateKey stateKey)
            {
                this.dispatcher = dispatcher;
                this.stateKey = stateKey;
            }

            /// <summary>
            /// Event that is invoked when an the specified eventKey is recieved.
            /// Callback parameters: (TParam data, TStateKey state, in TEventContext context).
            /// </summary>
            /// <param name="eventKey">Event name to listen for.</param>
            /// <typeparam name="TEventContext">Event context type passed through to the listener.</typeparam>
            /// <returns>The event action listener collection for the specifed state and eventKey.</returns>
            public EventActionListenerProxy<TParam, TStateKey, TEventContext> On<TEventContext>(TEventKey eventKey)
            {
                return new EventActionListenerProxy<TParam, TStateKey, TEventContext>(DictionaryExtensions.LazyInitialise(ref dispatcher._eventsState, eventKey, stateKey));
            }
        }

        //
        public readonly struct TransitionAPIProxy
        {
            private readonly ActionDispatcher<TStateKey, TEventKey, TParam> dispatcher;
            private readonly TStateKey stateKey;

            internal TransitionAPIProxy(ActionDispatcher<TStateKey, TEventKey, TParam> dispatcher, TStateKey stateKey)
            {
                this.dispatcher = dispatcher;
                this.stateKey = stateKey;
            }

            /// <summary>
            /// Event that is invoked when there is a transition between the two specified states.
            /// Callback parameters: (TParam data, TStateKey from, TStateKey to).
            /// </summary>
            /// <param name="to">The state which is transitioned to.</param>
            /// <returns>The action listener collection for the transition beween the two specified states.</returns>
            public ActionListener<TParam, TStateKey, TStateKey> To(TStateKey to)
            {
                return DictionaryExtensions.LazyInitialise(ref dispatcher._transitionState, stateKey, to);
            }
        }

        // general action listeners
        internal ActionListener<TParam, TStateKey> _enter = null;
        internal ActionListener<TParam, TStateKey> _exit = null;
        internal ActionListener<TParam, TStateKey, TStateKey> _transition = null;
        internal Dictionary<TEventKey, EventActionListener<TParam, TStateKey>> _events = null;

        // state action listeners
        internal Dictionary<TStateKey, ActionListener<TParam, TStateKey>> _enterState = null;
        internal Dictionary<TStateKey, ActionListener<TParam, TStateKey>> _exitState = null;
        internal Dictionary<TStateKey, Dictionary<TStateKey, ActionListener<TParam, TStateKey, TStateKey>>> _transitionState = null;
        internal Dictionary<TEventKey, Dictionary<TStateKey, EventActionListener<TParam, TStateKey>>> _eventsState = null;
        internal Dictionary<TStateKey, ActionListener<TParam, TStateKey>> _update = null;
        internal Dictionary<TStateKey, ActionListener<TParam, TStateKey>> _lateUpdate = null;
        internal Dictionary<TStateKey, ActionListener<TParam, TStateKey>> _fixedUpdate = null;

        // accessor functions / properties

        /// <summary>
        /// Event that is invoked when any state is entered.
        /// Callback parameters: (TParam data, TStateKey enter).
        /// </summary>
        /// <returns>The action listener collection for this event.</returns>
        public ActionListener<TParam, TStateKey> enter => Utils.LazyInitialise(ref _enter);

        /// <summary>
        /// Event that is invoked when any state is exited.
        /// Callback parameters: (TParam data, TStateKey exit).
        /// </summary>
        /// <returns>The action listener collection for this event.</returns>
        public ActionListener<TParam, TStateKey> exit => Utils.LazyInitialise(ref _exit);

        /// <summary>
        /// Event that is invoked when there is a transition between any two states.
        /// Callback parameters: (TParam data, TStateKey from, TStateKey to).
        /// </summary>
        /// <returns>The action listener collection for this event.</returns>
        public ActionListener<TParam, TStateKey, TStateKey> transition => Utils.LazyInitialise(ref _transition);

        /// <summary>
        /// Event that is invoked when an the specified eventKey is recieved.
        /// Callback parameters: (TParam data, TStateKey state, in TEventContext context).
        /// </summary>
        /// <param name="eventKey">Event name to listen for.</param>
        /// <typeparam name="TEventContext">Event context type passed through to the listener.</typeparam>
        /// <returns>The event action listener collection for the specifed eventKey.</returns>
        public EventActionListenerProxy<TParam, TStateKey, TEventContext> On<TEventContext>(TEventKey eventKey)
        {
            return new EventActionListenerProxy<TParam, TStateKey, TEventContext>(DictionaryExtensions.LazyInitialise(ref _events, eventKey));
        }

        /// <summary>
        /// Event that is invoked when the specified state is entered.
        /// Callback parameters: (TParam data, TStateKey enter).
        /// </summary>
        /// <param name="stateKey"></param>
        /// <returns>The action listener collection for this state event.</returns>
        public ActionListener<TParam, TStateKey> Enter(TStateKey stateKey) => DictionaryExtensions.LazyInitialise(ref _enterState, stateKey);
        
        /// <summary>
        /// Event that is invoked when the specified state is exited.
        /// Callback parameters: (TParam data, TStateKey enter).
        /// </summary>
        /// <param name="stateKey"></param>
        /// <returns>The action listener collection for this state event.</returns>
        public ActionListener<TParam, TStateKey> Exit(TStateKey stateKey) => DictionaryExtensions.LazyInitialise(ref _exitState, stateKey);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stateKey"></param>
        /// <returns></returns>
        public EventActionAPIProxy Event(TStateKey stateKey) => new EventActionAPIProxy(this, stateKey);

        /// <summary>
        /// Event that is invoked when there is a transition between the two specified states.
        /// Callback parameters: (TParam data, TStateKey from, TStateKey to).
        /// </summary>
        /// <param name="from">The state which is transitioned from.</param>
        /// <returns>The action listener collection for the transition beween the two specified states.</returns>
        public TransitionAPIProxy From(TStateKey from)
        {
            return new TransitionAPIProxy(this, from);
        }

        /// <summary>
        /// Event that is invoked during Update in the monobehaviour lifecycle on clients in the
        /// specified state.
        /// Callback parameters: (TParam data, TStateKey enter).
        /// </summary>
        /// <param name="stateKey"></param>
        /// <returns>The action listener collection for this state event.</returns>
        public ActionListener<TParam, TStateKey> Update(TStateKey stateKey) => DictionaryExtensions.LazyInitialise(ref _update, stateKey);

        /// <summary>
        /// Event that is invoked during LateUpdate in the monobehaviour lifecycle on clients in
        /// the specified state.
        /// Callback parameters: (TParam data, TStateKey enter).
        /// </summary>
        /// <param name="stateKey"></param>
        /// <returns>The action listener collection for this state event.</returns>
        public ActionListener<TParam, TStateKey> LateUpdate(TStateKey stateKey) => DictionaryExtensions.LazyInitialise(ref _lateUpdate, stateKey);

        /// <summary>
        /// Event that is invoked during FixedUpdate in the monobehaviour lifecycle on clients in
        /// the specified state.
        /// Callback parameters: (TParam data, TStateKey enter).
        /// </summary>
        /// <param name="stateKey"></param>
        /// <returns>The action listener collection for this state event.</returns>
        public ActionListener<TParam, TStateKey> FixedUpdate(TStateKey stateKey) => DictionaryExtensions.LazyInitialise(ref _fixedUpdate, stateKey);

        /// <summary>
        /// Adds a group of actions to a the given state.
        /// </summary>
        /// <param name="stateKey"></param>
        /// <param name="adder"></param>
        /// <typeparam name="TAdder"></typeparam>
        public void AddListeners<TAdder>(TStateKey stateKey, TAdder adder) where TAdder : IActionAdder<TStateKey, TEventKey, TParam>
        {
            adder.AddListeners(this, stateKey);
        }

        /// <summary>
        /// Removes a group of actions to a the given state.
        /// </summary>
        /// <param name="stateKey"></param>
        /// <param name="remover"></param>
        /// <typeparam name="TRemover"></typeparam>
        public void RemoveListeners<TRemover>(TStateKey stateKey, TRemover remover) where TRemover : IActionRemover<TStateKey, TEventKey, TParam>
        {
            remover.RemoveListeners(this, stateKey);
        }

        /// <summary>
        /// Removes all listeners from the dispatcher, effectively clearing it.
        /// </summary>
        public void RemoveAllListeners()
        {
            _enter.RemoveAllListeners();
            _exit.RemoveAllListeners();
            _transition.RemoveAllListeners();
            _events.Clear();
            _enterState.Clear();
            _exitState.Clear();
            _transitionState.Clear();
            _eventsState.Clear();
            _update.Clear();
            _lateUpdate.Clear();
            _fixedUpdate.Clear();

            _enter = null;
            _exit = null;
            _transition = null;
            _events = null;
            _enterState = null;
            _exitState = null;
            _transitionState = null;
            _eventsState = null;
            _update = null;
            _lateUpdate = null;
            _fixedUpdate = null;
        }
    }
}
