using System;
using System.Collections.Generic;

namespace Understated
{
    public readonly struct TransitionsAPIProxy<TStateKey, TEventKey, TParam>
    {
        //
        public readonly struct ToKeyOnEvent
        {
            private readonly Transitions<TStateKey, TEventKey, TParam> transitions;
            private readonly TStateKey from;
            private readonly TStateKey to;
            private readonly TEventKey eventKey;

            //
            internal ToKeyOnEvent(Transitions<TStateKey, TEventKey, TParam> transitions, TStateKey from, TStateKey to, TEventKey eventKey)
            {
                this.transitions = transitions;
                this.from = from;
                this.to = to;
                this.eventKey = eventKey;
            }

            //
            public EventTrigger<TParam, TEventContext> Always<TEventContext>()
            {
                return transitions.eventTriggers.TryGetValueOrInitialise(eventKey).TryGetValueOrInitialise(from).TryGetValueOrInitialise(to).AddListener(Triggers<TParam>.Event<TEventContext>.Always);
            }

            //
            public EventTrigger<TParam, TEventContext> When<TEventContext>(EventTrigger<TParam, TEventContext> eventTrigger)
            {
                return transitions.eventTriggers.TryGetValueOrInitialise(eventKey).TryGetValueOrInitialise(from).TryGetValueOrInitialise(to).AddListener(eventTrigger);
            }

            //
            public void Remove<TEventContext>(EventTrigger<TParam, TEventContext> eventTrigger)
            {
                transitions.eventTriggers.TryGetValueOrDefault(eventKey)?.TryGetValueOrDefault(from)?.TryGetValueOrDefault(to)?.RemoveListener(eventTrigger);
                DictionaryExtensions.RecursiveRemoveEmpty(transitions.eventTriggers, eventKey, from, to);
            }

            //
            public void RemoveAllEventTriggers()
            {
                transitions.eventTriggers.TryGetValueOrDefault(eventKey)?.TryGetValueOrDefault(from)?.TryGetValueOrDefault(to)?.RemoveAllListeners();
                DictionaryExtensions.RecursiveRemoveEmpty(transitions.eventTriggers, eventKey, from, to);
            }
        }

        //
        public readonly struct ToKey
        {
            private readonly Transitions<TStateKey, TEventKey, TParam> transitions;
            private readonly TStateKey from;
            private readonly TStateKey to;

            //
            internal ToKey(Transitions<TStateKey, TEventKey, TParam> transitions, TStateKey from, TStateKey to)
            {
                this.transitions = transitions;
                this.from = from;
                this.to = to;
            }

            //
            public PolledTrigger<TParam> Always()
            {
                return transitions.polledTriggers.TryGetValueOrInitialise(from).TryGetValueOrInitialise(to).AddListener(Triggers<TParam>.Polled.Always);
            }

            //
            public PolledTrigger<TParam> When(PolledTrigger<TParam> polledTrigger)
            {
                return transitions.polledTriggers.TryGetValueOrInitialise(from).TryGetValueOrInitialise(to).AddListener(polledTrigger);
            }

            //
            public ToKeyOnEvent On(TEventKey eventKey)
            {
                return new ToKeyOnEvent(transitions, from, to, eventKey);
            }

            //
            public TimeoutSelector<TStateKey, TParam> After(float timeout)
            {
                return transitions.timeoutSelectors.TryGetValueOrInitialise(from).Add(new TimeoutSelector<TStateKey, TParam>(timeout, Selectors<TStateKey, TParam>.Always(to)));
            }

            //
            public TimeoutSelector<TStateKey, TParam> After(Func<TParam, float> timeout)
            {
                return transitions.timeoutSelectors.TryGetValueOrInitialise(from).Add(new TimeoutSelector<TStateKey, TParam>(timeout, Selectors<TStateKey, TParam>.Always(to)));
            }

            //
            public void Remove(PolledTrigger<TParam> polledTrigger)
            {
                transitions.polledTriggers.TryGetValueOrDefault(from)?.TryGetValueOrDefault(to)?.RemoveListener(polledTrigger);
                DictionaryExtensions.RecursiveRemoveEmpty(transitions.polledTriggers, from, to);
            }

            //
            public void RemoveAllPolledTriggers()
            {
                transitions.polledTriggers.TryGetValueOrDefault(from)?.TryGetValueOrDefault(to)?.RemoveAllListeners();
                DictionaryExtensions.RecursiveRemoveEmpty(transitions.polledTriggers, from, to);
            }
        }

        //
        public readonly struct ToPolledSelector
        {
            private readonly Transitions<TStateKey, TEventKey, TParam> transitions;
            private readonly TStateKey from;
            private readonly PolledSelector<TParam, TStateKey> selector;

            //
            internal ToPolledSelector(Transitions<TStateKey, TEventKey, TParam> transitions, TStateKey from, PolledSelector<TParam, TStateKey> selector)
            {
                this.transitions = transitions;
                this.from = from;
                this.selector = selector;
            }

            //TODO: this call is not a neat part of the API
            public PolledSelector<TParam, TStateKey> Always()
            {
                return transitions.polledSelectors.TryGetValueOrInitialise(from).AddListener(selector);
            }

            //
            public TimeoutSelector<TStateKey, TParam> After(float timeout)
            {
                return transitions.timeoutSelectors.TryGetValueOrInitialise(from).Add(new TimeoutSelector<TStateKey, TParam>(timeout, selector));
            }

            //
            public TimeoutSelector<TStateKey, TParam> After(Func<TParam, float> timeout)
            {
                return transitions.timeoutSelectors.TryGetValueOrInitialise(from).Add(new TimeoutSelector<TStateKey, TParam>(timeout, selector));
            }
        }

        //
        public readonly struct ToEventSelector<TEventContext>
        {
            private readonly Transitions<TStateKey, TEventKey, TParam> transitions;
            private readonly TStateKey from;
            private readonly EventSelector<TParam, TEventContext, TStateKey> selector;

            //
            internal ToEventSelector(Transitions<TStateKey, TEventKey, TParam> transitions, TStateKey from, EventSelector<TParam, TEventContext, TStateKey> selector)
            {
                this.transitions = transitions;
                this.from = from;
                this.selector = selector;
            }

            public EventSelector<TParam, TEventContext, TStateKey> On(TEventKey eventKey)
            {
                return transitions.eventSelectors.TryGetValueOrInitialise(eventKey).TryGetValueOrInitialise(from).AddListener(selector);
            }
        }


        //
        private readonly Transitions<TStateKey, TEventKey, TParam> transitions;
        private readonly TStateKey from;

        //
        internal TransitionsAPIProxy(Transitions<TStateKey, TEventKey, TParam> transitions, TStateKey from)
        {
            this.transitions = transitions;
            this.from = from;
        }

        //
        public TransitionsAPIProxy<TStateKey, TEventKey, TParam>.ToKey To(TStateKey to)
        {
            return new ToKey(transitions, from, to);
        }

        //
        public TransitionsAPIProxy<TStateKey, TEventKey, TParam>.ToPolledSelector To(PolledSelector<TParam, TStateKey> polledSelector)
        {
            return new ToPolledSelector(transitions, from, polledSelector);
        }

        //
        public TransitionsAPIProxy<TStateKey, TEventKey, TParam>.ToEventSelector<TEventContext> To<TEventContext>(EventSelector<TParam, TEventContext, TStateKey> eventSelector)
        {
            return new ToEventSelector<TEventContext>(transitions, from, eventSelector);
        }

        //
        public void Remove(PolledSelector<TParam, TStateKey> polledSelector)
        {
            transitions.polledSelectors.TryGetValueOrDefault(from)?.RemoveListener(polledSelector);
            DictionaryExtensions.RecursiveRemoveEmpty(transitions.polledSelectors, from);
        }

        //
        public void RemoveAllPolledSelectors()
        {
            transitions.polledSelectors.TryGetValueOrDefault(from)?.RemoveAllListeners();
            DictionaryExtensions.RecursiveRemoveEmpty(transitions.polledSelectors, from);
        }

        //
        public void Remove<TEventContext>(TEventKey eventKey, EventSelector<TParam, TEventContext, TStateKey> eventSelector)
        {
            transitions.eventSelectors.TryGetValueOrDefault(eventKey).TryGetValueOrDefault(from)?.RemoveListener(eventSelector);
            DictionaryExtensions.RecursiveRemoveEmpty(transitions.eventSelectors, eventKey, from);
        }

        //
        public void RemoveAllEventSelectors(TEventKey eventKey)
        {
            transitions.eventSelectors.TryGetValueOrDefault(eventKey).TryGetValueOrDefault(from)?.RemoveAllListeners();
            DictionaryExtensions.RecursiveRemoveEmpty(transitions.eventSelectors, eventKey, from);
        }

        //
        public bool Remove(TimeoutSelector<TStateKey, TParam> timeoutSelector)
        {
            bool result = transitions.timeoutSelectors.TryGetValueOrDefault(from).Remove(timeoutSelector);
            DictionaryExtensions.RecursiveRemoveEmpty(transitions.timeoutSelectors, from);
            return result;
        }

        //
        public void RemoveAllTimeoutSelectors()
        {
            transitions.timeoutSelectors.TryGetValueOrDefault(from)?.RemoveAll();
            DictionaryExtensions.RecursiveRemoveEmpty(transitions.timeoutSelectors, from);
        }
    }

    //
    public class Transitions<TStateKey, TEventKey, TParam>
    {
        internal readonly struct PolledTransitions
        {
            public readonly Dictionary<TStateKey, PolledTriggerWrapper<TParam>> triggers;
            public readonly PolledSelectorWrapper<TParam, TStateKey> selectors;

            public PolledTransitions(Dictionary<TStateKey, PolledTriggerWrapper<TParam>> triggers, PolledSelectorWrapper<TParam, TStateKey> selectors)
            {
                this.triggers = triggers;
                this.selectors = selectors;
            }
        }

        internal readonly struct EventTransitions
        {
            public readonly Dictionary<TStateKey, EventTriggerWrapper<TParam>> triggers;
            public readonly EventSelectorWrapper<TParam, TStateKey> selectors;

            public EventTransitions(Dictionary<TStateKey, EventTriggerWrapper<TParam>> triggers, EventSelectorWrapper<TParam, TStateKey> selectors)
            {
                this.triggers = triggers;
                this.selectors = selectors;
            }
        }

        internal readonly struct TimeoutTransitions
        {
            public readonly TimeoutSelectorWrapper<TStateKey, TParam> selectors;

            public TimeoutTransitions(TimeoutSelectorWrapper<TStateKey, TParam> selectors)
            {
                this.selectors = selectors;
            }
        }


        // polled transitions - trigger map: <From, <To, Trigger>>, selector map: <From, Selector>
        internal Dictionary<TStateKey, Dictionary<TStateKey, PolledTriggerWrapper<TParam>>> polledTriggers = new Dictionary<TStateKey, Dictionary<TStateKey, PolledTriggerWrapper<TParam>>>(0);
        internal Dictionary<TStateKey, PolledSelectorWrapper<TParam, TStateKey>> polledSelectors = new Dictionary<TStateKey, PolledSelectorWrapper<TParam, TStateKey>>(0);

        // event transitions - trigger map: <Event, <From, <To, Trigger>>>, selector map: <Event, <From, Selector>>
        internal Dictionary<TEventKey, Dictionary<TStateKey, Dictionary<TStateKey, EventTriggerWrapper<TParam>>>> eventTriggers = new Dictionary<TEventKey, Dictionary<TStateKey, Dictionary<TStateKey, EventTriggerWrapper<TParam>>>>(0);
        internal Dictionary<TEventKey, Dictionary<TStateKey, EventSelectorWrapper<TParam, TStateKey>>> eventSelectors = new Dictionary<TEventKey, Dictionary<TStateKey, EventSelectorWrapper<TParam, TStateKey>>>(0);

        // timeout transitions - selector map: <From, Selector>
        internal Dictionary<TStateKey, TimeoutSelectorWrapper<TStateKey, TParam>> timeoutSelectors = new Dictionary<TStateKey, TimeoutSelectorWrapper<TStateKey, TParam>>(0);


        //
        public TransitionsAPIProxy<TStateKey, TEventKey, TParam> From(TStateKey from)
        {
            return new TransitionsAPIProxy<TStateKey, TEventKey, TParam>(this, from);
        }

        //
        internal PolledTransitions GetPolledTransitions(TStateKey from)
        {
            return new PolledTransitions(polledTriggers.TryGetValueOrDefault(from), polledSelectors.TryGetValueOrDefault(from));
        }

        //
        internal EventTransitions GetEventTransitions(TEventKey eventKey, TStateKey from)
        {
            return new EventTransitions(eventTriggers.TryGetValueOrDefault(eventKey)?.TryGetValueOrDefault(from), eventSelectors.TryGetValueOrDefault(eventKey)?.TryGetValueOrDefault(from));
        }

        //
        internal TimeoutTransitions GetTimeoutTransitions(TStateKey from)
        {
            return new TimeoutTransitions(timeoutSelectors.TryGetValueOrDefault(from));
        }

        //
        internal bool HasPolledTransitions()
        {
            return polledTriggers.Count > 0 || polledSelectors.Count > 0;
        }

        //
        internal bool HasEventTransitions()
        {
            return eventTriggers.Count > 0 || eventSelectors.Count > 0;
        }

        //
        internal bool HasTimeoutTransitions()
        {
            return timeoutSelectors.Count > 0;
        }
    }
}
