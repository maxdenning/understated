using System;
using System.Collections;

namespace Understated
{
    /*
    */
    public abstract class DelegateWrapper<TDelegate>
        where TDelegate : Delegate
    {
        protected TDelegate wrappedDelegate { get; private set; } = null;

        public TDelegate AddListener(TDelegate listener)
        {
            wrappedDelegate = Delegate.Combine(wrappedDelegate, listener) as TDelegate;
            return listener;
        }

        public void RemoveListener(TDelegate listener)
        {
            // RemoveAll returns null if wrappedDelegate becomes empty
            wrappedDelegate = Delegate.RemoveAll(wrappedDelegate, listener) as TDelegate;
        }

        public void RemoveAllListeners()
        {
            wrappedDelegate = null;
        }
    }

    /*
    */
    internal abstract class CachedDelegateWrapper<TDelegate> : ICollection
        where TDelegate : Delegate
    {
        public int Count { get; private set; } = 0;
        protected TDelegate wrappedDelegate { get; private set; } = null;
        private static readonly TDelegate[] EMPTY_DELEGATE_LIST = new TDelegate[0];
        private TDelegate[] cachedInvocationList = EMPTY_DELEGATE_LIST;  // cached list from wrappedDelegate

        public TDelegate AddListener(TDelegate listener)
        {
            wrappedDelegate = Delegate.Combine(wrappedDelegate, listener) as TDelegate;
            RebuildCache();
            return listener;
        }

        public void RemoveListener(TDelegate listener)
        {
            if (wrappedDelegate != null)
            {
                wrappedDelegate = Delegate.RemoveAll(wrappedDelegate, listener) as TDelegate;  // returns null if wrappedDelegate becomes empty
                RebuildCache();
            }
        }

        public void RemoveAllListeners()
        {
            wrappedDelegate = null;
            RebuildCache();
        }

        // short lifetime, unsafe
        protected TDelegate[] GetInvocationList()
        {
            return cachedInvocationList;
        }

        private void RebuildCache()
        {
            // invalidate cache
            Array.Clear(cachedInvocationList, 0, Count);
            if (wrappedDelegate == null)
            {
                //TODO: reset allocated cache length to 0 if wrapped delegate becomes null?
                Count = 0;
                return;
            }

            //
            if (typeof(TDelegate) == typeof(Delegate))
            {
                // shortcut conversion if it is unnecessary
                cachedInvocationList = wrappedDelegate.GetInvocationList() as TDelegate[];
                Count = cachedInvocationList.Length;
            }
            else
            {
                // copy
                Delegate[] invocationList = wrappedDelegate.GetInvocationList();

                // allocate if cache is too small
                if (cachedInvocationList.Length < invocationList.Length)
                {
                    cachedInvocationList = new TDelegate[invocationList.Length];
                }

                // convert Delegate to TDelegate
                Count = invocationList.Length;
                for (int i = 0; i < Count; i++)
                {
                    if (invocationList[i] is TDelegate typedDelegate)
                    {
                        cachedInvocationList[i] = typedDelegate;
                    }
                    else
                    {
                        throw new InvalidCastException("Delegate wrapper type mismatch");
                    }
                }
            }
        }

        // ICollection interface
        public bool IsReadOnly => cachedInvocationList.IsReadOnly;

        public bool IsSynchronized => cachedInvocationList.IsSynchronized;

        public object SyncRoot => cachedInvocationList.SyncRoot;

        public void CopyTo(Array array, int index) => cachedInvocationList.CopyTo(array, index);

        IEnumerator IEnumerable.GetEnumerator() => cachedInvocationList.GetEnumerator();
    }

    /*
    */
    public abstract class GenericDelegateWrapper : DelegateWrapper<Delegate>
    {
        public TListener AddListener<TListener>(TListener listener)
            where TListener : Delegate
        {
            if (wrappedDelegate == null || wrappedDelegate is TListener)
            {
                return base.AddListener(listener) as TListener;
            }
            else
            {
                // enforce one TListener type per delegate
                throw new ArgumentException("TListener does not match the type associated with existing delegates for this event", "TListener");
            }
        }

        public void RemoveListener<TListener>(TListener listener)
            where TListener : Delegate
        {
            if (wrappedDelegate is TListener)
            {
                base.RemoveListener(listener);
            }
            else if (wrappedDelegate != null)
            {
                // enforce one TListener type per delegate
                throw new ArgumentException("TListener does not match the type associated with existing delegates for this event", "TListener");
            }
        }
    }

    /*
    */
    internal abstract class CachedGenericDelegateWrapper : CachedDelegateWrapper<Delegate>
    {
        public TListener AddListener<TListener>(TListener listener)
            where TListener : Delegate
        {
            if (wrappedDelegate == null || wrappedDelegate is TListener)
            {
                return base.AddListener(listener) as TListener;
            }
            else
            {
                // enforce one TListener type per delegate
                throw new ArgumentException("TListener does not match the type associated with existing delegates for this event", "TListener");
            }
        }

        public void RemoveListener<TListener>(TListener listener)
            where TListener : Delegate
        {
            if (wrappedDelegate is TListener)
            {
                base.RemoveListener(listener);
            }
            else if (wrappedDelegate != null)
            {
                // enforce one TListener type per delegate
                throw new ArgumentException("TListener does not match the type associated with existing delegates for this event", "TListener");
            }
        }
    }
}
