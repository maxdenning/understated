using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Understated
{
    //TODO: add repeating timeouts, e.g. .After(...).Repeat(); or .After(...).Once();
    public class TimeoutSelector<TStateKey, TParam>
    {
        internal readonly float durationValue;
        internal readonly Func<TParam, float> durationCallback;
        internal readonly PolledSelector<TParam, TStateKey> selector;

        internal TimeoutSelector(float duration, PolledSelector<TParam, TStateKey> selector)
        {
            this.durationValue = duration;
            this.durationCallback = null;
            this.selector = selector;
        }

        internal TimeoutSelector(Func<TParam, float> duration, PolledSelector<TParam, TStateKey> selector)
        {
            this.durationValue = -1;
            this.durationCallback = duration;
            this.selector = selector;
        }
    }

    //
    public class TimeoutManager<TClient, TStateKey, TParam>
    {
        //
        private readonly struct Timeout
        {
            public Timeout(int id, double endTime, TClient client, TParam data, PolledSelector<TParam, TStateKey> selector)
            {
                this.id = id;
                this.endTime = endTime;
                this.client = client;
                this.data = data;
                this.selector = selector;
            }

            public readonly int id;
            public readonly double endTime;
            public readonly TClient client;
            public readonly TParam data;
            public readonly PolledSelector<TParam, TStateKey> selector;
        }

        //
        private class SharedFlag
        {
            public bool value = false;
            public SharedFlag(bool value)
            {
                this.value = value;
            }
        }

        //
        public readonly Coroutine coroutine;
        private SharedFlag exit = new SharedFlag(false);
        private List<Timeout> timeouts = new List<Timeout>(0);
        private int idCounter = 0;

        /*
        */
        public TimeoutManager(MonoBehaviour coroutineOwner, Action<TClient, int, bool, TStateKey> timeoutComplete)
        {
            this.coroutine = coroutineOwner.StartCoroutine(TimeoutSelectorManagerCoroutine(exit, timeouts, timeoutComplete));
        }

        /*
        */
        ~TimeoutManager()
        {
            Stop();
        }

        /*
        */
        public bool IsCoroutineRunning()
        {
            return !exit.value;
        }

        /*
        */
        public int Add(TimeoutSelector<TStateKey, TParam> timeoutSelector, TClient client, TParam data, double timeNow)
        {
            double endTime;
            if (timeoutSelector.durationCallback == null)
            {
                endTime = timeNow + (double)timeoutSelector.durationValue;
            }
            else
            {
                endTime = timeNow + (double)timeoutSelector.durationCallback(data);
            }

            int id = NextID();
            timeouts.Add(new Timeout(id, endTime, client, data, timeoutSelector.selector));
            return id;
        }

        /*
        */
        public bool Remove(int id)
        {
            int index = timeouts.FindIndex((t) => (t.id == id));  //TODO: this is slow
            if (index > -1)
            {
                timeouts.UnstableRemoveAt(index);
                return true;
            }
            return false;
        }

        /*
        */
        public void Stop()
        {
            exit.value = true;
        }

        /*
        */
        private int NextID()
        {
            return idCounter++;
        }

        /*
        */
        private static IEnumerator TimeoutSelectorManagerCoroutine(SharedFlag exit, List<Timeout> timeouts, Action<TClient, int, bool, TStateKey> onTimeoutComplete)
        {
            double timeNow = 0;
            TStateKey nextState = default(TStateKey);

            //TODO: use sorted list and search for time cut off?
            while (!exit.value)
            {
                timeNow = Time.timeAsDouble;
                for (int i = timeouts.Count - 1; i >= 0 ; i--)
                {
                    if (timeNow >= timeouts[i].endTime)
                    {
                        // timeout is complete, select next state
                        bool isStateChangeRequested = timeouts[i].selector(timeouts[i].data, ref nextState);
                        onTimeoutComplete(timeouts[i].client, timeouts[i].id, isStateChangeRequested, nextState);

                        // no longer track this timeout
                        timeouts.UnstableRemoveAt(i);
                    }
                }
                yield return null;
            }
        }
    }
}
