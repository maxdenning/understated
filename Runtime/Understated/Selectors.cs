using System;

namespace Understated
{
    public static class Selectors<TStateKey, TParam>
    {
        //
        public static PolledSelector<TParam, TStateKey> Weighted(params (float, TStateKey)[] pairs)
        {
            // sort pairs in descending order of chance (i.e. highest probability first)
            Array.Sort(pairs, (a, b) => a.Item1 < b.Item1 ? -1 : a.Item1 == b.Item1 ? 0 : 1);
            Array.Reverse(pairs);

            // cumulatively sum probabilities to create upper boundaries, e.g. [0.3, 0.2, 0.1] -> [0.3, 0.5, 0.6]
            for (int i = 1; i < pairs.Length; i++)
            {
                pairs[i].Item1 += pairs[i - 1].Item1;
            }

            return (TParam data, ref TStateKey nextState) => {
                float choice = UnityEngine.Random.Range(0.0f, pairs[pairs.Length - 1].Item1);
                foreach (var item in pairs)
                {
                    if (choice <= item.Item1)
                    {
                        nextState = item.Item2;
                        return true;
                    }
                }
                return false;
            };
        }

        //
        public static PolledSelector<TParam, TStateKey> Random(params TStateKey[] states)
        {
            return (TParam data, ref TStateKey nextState) => {
                nextState = states[UnityEngine.Random.Range(0, states.Length)];
                return true;
            };
        }

        //
        public static PolledSelector<TParam, TStateKey> Ternary(Predicate<TParam> predicate, TStateKey ifTrue, TStateKey ifFalse)
        {
            return (TParam data, ref TStateKey nextState) => {
                nextState = predicate.Invoke(data) ? ifTrue : ifFalse;
                return true;
            };
        }

        //
        public static PolledSelector<TParam, TStateKey> Always(TStateKey to)
        {
            return (TParam data, ref TStateKey nextState) => {
                nextState = to;
                return true;
            };
        }
    }

    public static class Triggers<TParam>
    {
        //
        public static class Polled
        {
            public static PolledTrigger<TParam> Always = (TParam data) => true;
        }

        //
        public static class Event<TEventContext>
        {
            public static EventTrigger<TParam, TEventContext> Always = (TParam data, in TEventContext context) => true;
        }
    }
}
