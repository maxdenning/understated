using System;
using System.Collections;
using System.Collections.Generic;

namespace Understated
{
    internal static class Utils
    {
        public static TValue LazyInitialise<TValue>(ref TValue value)
            where TValue : new()
        {
            if (value == null)
            {
                value = new TValue();
            }
            return value;
        }
    }

    internal static class ListExtensions
    {
        public static void UnstableRemoveAt<T>(this List<T> list, int index)
        {
            list[index] = list[list.Count - 1];  // overwrite [index] with final element
            list.RemoveAt(list.Count - 1);  // remove final element
        }
    }

    internal static class DictionaryExtensions
    {
        public static TValue TryGetValueOrInitialise<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
            where TValue : class, new()
        {
            TValue value;
            if (!dictionary.TryGetValue(key, out value))
            {
                value = new TValue();
                dictionary.Add(key, value);
            }
            return value;
        }

        public static TValue TryGetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
            where TValue : class
        {
            TValue result;
            dictionary.TryGetValue(key, out result);
            return result;
        }

        public static void Deconstruct<T1, T2>(this KeyValuePair<T1, T2> tuple, out T1 key, out T2 value)
        {
            key = tuple.Key;
            value = tuple.Value;
        }

        public static TValue LazyInitialise<TKey, TValue>(ref Dictionary<TKey, TValue> dict, TKey key)
            where TValue : class, new()
        {
            TValue value;
            if (dict == null)
            {
                value = new TValue();
                dict = new Dictionary<TKey, TValue>(1){ {key, value} };
            }
            else if (!dict.TryGetValue(key, out value))
            {
                value = new TValue();
                dict.Add(key, value);
            }
            return value;
        }

        public static TValue LazyInitialise<TKey0, TKey1, TValue>(ref Dictionary<TKey0, Dictionary<TKey1, TValue>> dict, TKey0 key0, TKey1 key1)
            where TValue : class, new()
        {
            Dictionary<TKey1, TValue> inner = LazyInitialise(ref dict, key0);
            TValue value = LazyInitialise(ref inner, key1);
            return value;
        }

        public static void RecursiveRemoveEmpty<TDict>(TDict dict, params object[] keys)
            where TDict : IDictionary
        {
            RecursiveRemoveEmpty(dict, 0, keys);
        }

        private static void RecursiveRemoveEmpty<TDict, TKey>(TDict dict, int depth, params TKey[] keys)
            where TDict : IDictionary
        {
            if (depth >= keys.Length)
            {
                return;
            }

            TKey key = keys[depth];
            if (dict.Contains(key))
            {
                var next = dict[key];

                if (next is IDictionary nextDict)
                {
                    RecursiveRemoveEmpty(nextDict, ++depth, keys);
                }

                if (next is ICollection nextCollection && nextCollection.Count <= 0)
                {
                    dict.Remove(key);
                }
            }
        }
    }

    /*
        API extensions for ActionDispatcher
    */
    public static class ActionListenerExtensions
    {
        public static Action<ClientCollection<TArg0>, TArg1> AddListener<TArg0, TArg1>(this ActionListener<ClientCollection<TArg0>, TArg1> wrapper, Action<TArg0, TArg1> listener)
        {
            return wrapper.AddListener((ClientCollection<TArg0> arg0, TArg1 arg1) => {
                foreach (TArg0 item in arg0)
                {
                    listener.Invoke(item, arg1);
                }
            });
        }

        public static Action<ClientCollection<TArg0>, TArg1, TArg2> AddListener<TArg0, TArg1, TArg2>(this ActionListener<ClientCollection<TArg0>, TArg1, TArg2> wrapper, Action<TArg0, TArg1, TArg2> listener)
        {
            return wrapper.AddListener((ClientCollection<TArg0> arg0, TArg1 arg1, TArg2 arg2) => {
                foreach (TArg0 item in arg0)
                {
                    listener.Invoke(item, arg1, arg2);
                }
            });
        }
    }

    /*
        API extensions for ActionDispatcher
    */
    public static class EventActionListenerExtensions
    {
        public static EventAction<ClientCollection<TArg0>, TArg1, TEventContext> AddListener<TArg0, TArg1, TEventContext>(this EventActionListenerProxy<ClientCollection<TArg0>, TArg1, TEventContext> proxy, EventAction<TArg0, TArg1, TEventContext> listener)
        {
            return proxy.AddListener((ClientCollection<TArg0> arg0, TArg1 arg1, in TEventContext arg2) => {
                foreach (TArg0 item in arg0)
                {
                    listener.Invoke(item, arg1, arg2);
                }
            });
        }

        public static EventAction<ClientCollection<TArg0>, TArg1, TEventContext> AddListener<TArg0, TArg1, TEventContext>(this EventActionListenerProxy<ClientCollection<TArg0>, TArg1, TEventContext> wrapper, Action<TArg0, TArg1> listener)
        {
            return wrapper.AddListener((ClientCollection<TArg0> arg0, TArg1 arg1, in TEventContext arg2) => {
                foreach (TArg0 item in arg0)
                {
                    listener.Invoke(item, arg1);
                }
            });
        }
    }
}
