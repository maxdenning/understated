using UnityEngine;
using UnityEngine.UI;
using Understated;

namespace GettingStarted
{
    public class StateLabelUI : MonoBehaviour
    {
        [SerializeField]
        private MySingleController singleController;

        [SerializeField]
        private MyMultiController multiController;

        [SerializeField]
        private MyMultiClient multiClient;

        [SerializeField]
        private Text textUI;

        [SerializeField]
        private Vector3 offset;

        private Transform follow = null;

        private void Start()
        {
            if (singleController != null)
            {
                singleController.actions.enter.AddListener((data, enter) => textUI.text = enter.ToString());
                follow = singleController.transform;
            }
            else
            {
                multiController.actions.enter.AddListener((MyData data, MyStates enter) => { if (data == multiClient.data) textUI.text = enter.ToString(); });
                follow = multiClient.transform;
            }
        }

        private void Update()
        {
            textUI.transform.position = Camera.main.WorldToScreenPoint(follow.position + offset);
        }
    }
}
