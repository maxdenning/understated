using UnityEngine;
using Understated;

namespace GettingStarted
{
    // These are all the possible states that a client of the state machine can be in.
    public enum MyStates
    {
        Idle = 0,  // defines the default state (i.e. default(MyStates) == MyStates.Idle)
        Moving
    }

    // These are the names of user-generated events which we will emit elsewhere in the code.
    public enum MyEvents
    {
        ChangeColour
    }

    [System.Serializable]
    public class MyData
    {
        public Transform transform;  // used to move the agent around
        public SpriteRenderer sprite;  // used to change the colour of the agent
        public Vector3 moveSpeed = new Vector3(4f, 4f, 0f);  // the speed of the agent
        public Vector3 moveDirection = Vector3.up;  // the direction of the agent
    }

    // use CreateAssetMenu to add an option to the create menu in the Unity Editor project window
    [CreateAssetMenu(fileName = "MyMachineSchema", menuName = "Understated/GettingStarted/MyMachineSchema")]
    public class MyMachineSchema : Understated.Schema<MyStates, MyEvents, MyData>
    {
        public MyMachineSchema()
        {
            actions.Enter(MyStates.Moving).AddListener(TurnAction);
            actions.Update(MyStates.Moving).AddListener(MoveAction);

            // change colour only when Idle and a ChangeColor event is received
            actions.Event(MyStates.Idle).On<Color>(MyEvents.ChangeColour).AddListener(ChangeColourAction);

            // log whenever an agent enters a new state
            actions.enter.AddListener((MyData data, MyStates state) => Debug.Log($"{data.transform.name} enter {state}"));

            transitions.From(MyStates.Idle).To(MyStates.Moving).After(1f);  // 1 second
            transitions.From(MyStates.Moving).To(MyStates.Idle).After(3f);  // 3 seconds
        }

        public static void TurnAction(MyData data, MyStates state)
        {
            // randomly pick a new direction
            data.moveDirection = Vector3.Normalize(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f));
        }

        public static void MoveAction(MyData data, MyStates state)
        {
            // move in the direction the agent has chosen
            data.transform.position += Vector3.Scale(data.moveSpeed, data.moveDirection) * Time.deltaTime;
        }

        public static void ChangeColourAction(MyData data, MyStates state, in Color eventContext)
        {
            // set the agent colour to the given value
            data.sprite.color = eventContext;
        }
    }
}
