using System;
using UnityEngine;
using Understated;

namespace UnderstatedRuntimeTests
{
    public enum TestStates
    {
        One = 1, Two = 2, Three = 3, Four = 4, Five = 5,
    }

    public enum TestEvents
    {
        ChangeStateToFour,
        ChangeStateToFive,
        SetValue,
    }

    [Serializable]
    public class TestData
    {
        [HideInInspector]
        public volatile bool rv1 = false;
        
        [HideInInspector]
        public volatile bool rv2 = true;

        // schema actions
        public int schemaEnter = 0;
        public int schemaExit = 0;
        public int schemaTransition = 0;
        public int schemaEvent = 0;

        public int schemaEnterState = 0;
        public int schemaExitState = 0;
        public int schemaTransitionState = 0;
        public int schemaEventState = 0;

        public int schemaUpdateState = 0;
        public int schemaFixedUpdateState = 0;
        public int schemaLateUpdateState = 0;

        // controller actions
        public int controllerEnter = 0;
        public int controllerExit = 0;
        public int controllerTransition = 0;
        public int controllerEvent = 0;

        public int controllerEnterState = 0;
        public int controllerExitState = 0;
        public int controllerTransitionState = 0;
        public int controllerEventState = 0;

        public int controllerUpdateState = 0;
        public int controllerFixedUpdateState = 0;
        public int controllerLateUpdateState = 0;
    }

    [CreateAssetMenu(fileName = "TestSchema", menuName = "Understated/RuntimeTests/TestSchema")]
    public class TestSchema : Understated.Schema<TestStates, TestEvents, TestData>
    {
        public TestSchema()
        {
            //
            Schema_SetValues(actions);
            Schema_SetStateValues(actions, TestStates.One, TestStates.Two);
            Schema_SetStateValues(actions, TestStates.Two, TestStates.Three);
            Schema_SetStateValues(actions, TestStates.Three, TestStates.Four);
            Schema_SetStateValues(actions, TestStates.Four, TestStates.Five);
            Schema_SetStateValues(actions, TestStates.Five, TestStates.One);

            // polled triggers
            transitions.From(TestStates.One).To(TestStates.Two).When((data) => data.rv1);
            transitions.From(TestStates.One).To(TestStates.Two).When((data) => data.rv1);
            transitions.From(TestStates.One).To(TestStates.Two).When((data) => data.rv2);

            // polled selectors
            transitions.From(TestStates.Two).To((TestData data, ref TestStates next) => { return data.rv1; }).Always();
            transitions.From(TestStates.Two).To((TestData data, ref TestStates next) => { return data.rv1; }).Always();
            transitions.From(TestStates.Two).To((TestData data, ref TestStates next) => { next = TestStates.Three; return data.rv2; }).Always();

            // event triggers
            transitions.From(TestStates.Three).To(TestStates.Four).On(TestEvents.ChangeStateToFour).When((TestData data, in TestStates context) => data.rv1);
            transitions.From(TestStates.Three).To(TestStates.Four).On(TestEvents.ChangeStateToFour).When((TestData data, in TestStates context) => data.rv1);
            transitions.From(TestStates.Three).To(TestStates.Four).On(TestEvents.ChangeStateToFour).When((TestData data, in TestStates context) => data.rv2 && context == TestStates.Four);

            // event selectors
            transitions.From(TestStates.Four).To((TestData data, in TestStates context, ref TestStates next) => { return data.rv1; }).On(TestEvents.ChangeStateToFive);
            transitions.From(TestStates.Four).To((TestData data, in TestStates context, ref TestStates next) => { return data.rv1; }).On(TestEvents.ChangeStateToFive);
            transitions.From(TestStates.Four).To((TestData data, in TestStates context, ref TestStates next) => { next = TestStates.Five; return data.rv2 && context == TestStates.Five; }).On(TestEvents.ChangeStateToFive);

            // // timeout selector
            // transitions.From(TestStates.Three).To(TestStates.Four).After(0.0001f);
            // transitions.From(TestStates.Three).To(TestStates.Four).After(0.0001f);
            // transitions.From(TestStates.Three).To(TestStates.Four).After(0.0001f);

            // // timeout selector
            // transitions.From(TestStates.Four).To((TestData data, ref TestStates next) => { return data.rv1; }).After(0.0001f);
            // transitions.From(TestStates.Four).To((TestData data, ref TestStates next) => { return data.rv1; }).After(0.0001f);
            // transitions.From(TestStates.Four).To((TestData data, ref TestStates next) => { next = TestStates.Five; return data.rv2; }).After(0.0001f);
        }

        //
        public static void Schema_SetValues(ActionDispatcher<TestStates, TestEvents, ClientCollection<TestData>> actions)
        {
            actions.enter.AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.schemaEnter = (int)state;
                }
            });

            actions.exit.AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.schemaExit = (int)state;
                }
            });

            actions.transition.AddListener((ClientCollection<TestData> collection, TestStates from, TestStates to) => {
                foreach (var data in collection)
                {
                    data.schemaTransition = (int)from;
                }
            });

            actions.On<int>(TestEvents.SetValue).AddListener((ClientCollection<TestData> collection, TestStates state, in int context) => {
                if (context == (int)state)
                {
                    foreach (var data in collection)
                    {
                        data.schemaEvent = (int)state;
                    }
                }
            });
        }

        public static void Schema_SetStateValues(ActionDispatcher<TestStates, TestEvents, ClientCollection<TestData>> actions, TestStates state, TestStates to)
        {
            actions.Enter(state).AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.schemaEnterState = (int)state;
                }
            });

            actions.Exit(state).AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.schemaExitState = (int)state;
                }
            });

            actions.Event(state).On<int>(TestEvents.SetValue).AddListener((ClientCollection<TestData> collection, TestStates state, in int context) => {
                if (context == (int)state)
                {
                    foreach (var data in collection)
                    {
                        data.schemaEventState = (int)state;
                    }
                }
            });

            actions.Update(state).AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.schemaUpdateState = (int)state;
                }
            });

            actions.FixedUpdate(state).AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.schemaFixedUpdateState = (int)state;
                }
            });

            actions.LateUpdate(state).AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.schemaLateUpdateState = (int)state;
                }
            });

            actions.From(state).To(to).AddListener((ClientCollection<TestData> collection, TestStates f, TestStates t) => {
                if (f == state && t == to)
                {
                    foreach (var data in collection)
                    {
                        data.schemaTransitionState = (int)state;
                    }
                }
            });
        }

        //
        public static void Controller_SetValues(ActionDispatcher<TestStates, TestEvents, ClientCollection<TestData>> actions)
        {
            actions.enter.AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.controllerEnter = (int)state;
                }
            });

            actions.exit.AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.controllerExit = (int)state;
                }
            });

            actions.On<int>(TestEvents.SetValue).AddListener((ClientCollection<TestData> collection, TestStates state, in int context) => {
                if (context == (int)state)
                {
                    foreach (var data in collection)
                    {
                        data.controllerEvent = (int)state;
                    }
                }
            });

            actions.transition.AddListener((ClientCollection<TestData> collection, TestStates from, TestStates to) => {
                foreach (var data in collection)
                {
                    data.controllerTransition = (int)from;
                }
            });
        }

        public static void Controller_SetStateValues(ActionDispatcher<TestStates, TestEvents, ClientCollection<TestData>> actions, TestStates state, TestStates to)
        {
            actions.Enter(state).AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.controllerEnterState = (int)state;
                }
            });

            actions.Exit(state).AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.controllerExitState = (int)state;
                }
            });

            actions.Event(state).On<int>(TestEvents.SetValue).AddListener((ClientCollection<TestData> collection, TestStates state, in int context) => {
                if (context == (int)state)
                {
                    foreach (var data in collection)
                    {
                        data.controllerEventState = (int)state;
                    }
                }
            });

            actions.Update(state).AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.controllerUpdateState = (int)state;
                }
            });

            actions.FixedUpdate(state).AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.controllerFixedUpdateState = (int)state;
                }
            });

            actions.LateUpdate(state).AddListener((ClientCollection<TestData> collection, TestStates state) => {
                foreach (var data in collection)
                {
                    data.controllerLateUpdateState = (int)state;
                }
            });

            actions.From(state).To(to).AddListener((ClientCollection<TestData> collection, TestStates f, TestStates t) => {
                if (f == state && t == to)
                {
                    foreach (var data in collection)
                    {
                        data.controllerTransitionState = (int)state;
                    }
                }
            });
        }

        //
        public static void Controller_SetValues(ActionDispatcher<TestStates, TestEvents, TestData> actions)
        {
            actions.enter.AddListener((TestData data, TestStates state) => data.controllerEnter = (int)state);
            actions.exit.AddListener((TestData data, TestStates state) => data.controllerExit = (int)state);
            actions.On<int>(TestEvents.SetValue).AddListener((TestData data, TestStates state, in int context) => { if (context == (int)state) data.controllerEvent = (int)state; });
            actions.transition.AddListener((TestData data, TestStates from, TestStates to) => data.controllerTransition = (int)from);
        }

        public static void Controller_SetStateValues(ActionDispatcher<TestStates, TestEvents, TestData> actions, TestStates state, TestStates to)
        {
            actions.Enter(state).AddListener((TestData data, TestStates state) => data.controllerEnterState = (int)state);
            actions.Exit(state).AddListener((TestData data, TestStates state) => data.controllerExitState = (int)state);
            actions.Event(state).On<int>(TestEvents.SetValue).AddListener((TestData data, TestStates state, in int context) => { if (context == (int)state) data.controllerEventState = (int)state; });
            actions.Update(state).AddListener((TestData data, TestStates state) =>  data.controllerUpdateState = (int)state);
            actions.FixedUpdate(state).AddListener((TestData data, TestStates state) => data.controllerFixedUpdateState = (int)state);
            actions.LateUpdate(state).AddListener((TestData data, TestStates state) => data.controllerLateUpdateState = (int)state);
            actions.From(state).To(to).AddListener((TestData data, TestStates f, TestStates t) => { if (f == state && t == to) data.controllerTransitionState = (int)state; });
        }
    }
}
